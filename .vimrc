syntax on
set cursorline
set number
set tabstop=4
set expandtab
set clipboard=unnamed
set laststatus=2
set ruler
set mouse=nicr
packadd! dracula
let g:dracula_italic = 0
colorscheme dracula
set backupdir=~/.vim-tmp
set directory=~/.vim-tmp
set redrawtime=10000
set backspace=indent,eol,start
set wrap
let g:netrw_banner=0
let g:netrw_winsize=20
let g:netrw_liststyle=3


:set splitbelow
:set splitright

call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'dart-lang/dart-vim-plugin'

call plug#end()
