# Install First

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew install zsh git tmux lazygit kubectl kubectx helm fzf wget jq yq docker docker-compose docker-machine virtualbox
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
brew install --cask font-roboto-mono-nerd-font
chmod 755 ~/.tmux
$(brew --prefix)/opt/fzf/install
```

## In vim
```
:PlugInstall
```

## virtualbox

Remember to open SystemPreference and allow installation
